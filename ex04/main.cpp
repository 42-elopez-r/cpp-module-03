/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 19:39:30 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/11 19:58:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"
#include <iostream>

using std::cout; using std::endl;

int
main()
{
	SuperTrap sup1("SUP1");
	NinjaTrap n1("N1");
	ClapTrap c1("C1");
	ScavTrap s1("S1");
	FragTrap f1("F1");

	sup1.vaulthunter_dot_exe("C1");
	sup1.ninjaShoebox(c1);
	sup1.ninjaShoebox(s1);
	sup1.ninjaShoebox(f1);
	sup1.ninjaShoebox(n1);

	return (0);
}
