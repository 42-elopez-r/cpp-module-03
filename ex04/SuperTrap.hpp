/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 17:46:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/11 20:07:41 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPER_TRAP_HPP
#define SUPER_TRAP_HPP

#include "FragTrap.hpp"
#include "NinjaTrap.hpp"
#include <string>

using std::string;

class SuperTrap: public FragTrap, public NinjaTrap
{
	public:
		SuperTrap();
		SuperTrap(const string& name);
		SuperTrap(const SuperTrap& superTrap);
		~SuperTrap();
		SuperTrap& operator=(const SuperTrap& superTrap);
};

#endif
