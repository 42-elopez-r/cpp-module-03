/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 17:46:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/11 20:07:56 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAG_TRAP_HPP
#define FRAG_TRAP_HPP

#include "ClapTrap.hpp"
#include <string>

using std::string;

class FragTrap: public virtual ClapTrap
{
	public:
		FragTrap();
		FragTrap(const string& name);
		FragTrap(const FragTrap& fragTrap);
		~FragTrap();
		FragTrap& operator=(const FragTrap& fragTrap);
		void vaulthunter_dot_exe(const string& target);
};

#endif
