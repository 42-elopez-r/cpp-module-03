/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 17:46:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/31 20:25:19 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAV_TRAP_HPP
#define SCAV_TRAP_HPP

#include "ClapTrap.hpp"
#include <string>

using std::string;

class ScavTrap: public ClapTrap
{
	public:
		ScavTrap();
		ScavTrap(const string& name);
		ScavTrap(const ScavTrap& scavTrap);
		~ScavTrap();
		ScavTrap& operator=(const ScavTrap& scavTrap);
		void challengeNewcomer(const string& target);
};

#endif
