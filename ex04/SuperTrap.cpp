/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/11 19:49:13 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

SuperTrap::SuperTrap()
{
	cout << "=== Default SuperTrap constructor ===" << endl;
	hitPoints = 60;
	maxHitPoints = 60;
	energyPoints = 120;
	maxEnergyPoints = 120;
	level = 1;
	meleeAttackDamage = 60;
	rangedAttackDamage = 5;
	armorDamageReduction = 0;
}

SuperTrap::SuperTrap(const string& name) : ClapTrap(name)
{
	cout << "=== Parametrized SuperTrap constructor ===" << endl;
	hitPoints = 60;
	maxHitPoints = 60;
	energyPoints = 120;
	maxEnergyPoints = 120;
	level = 1;
	meleeAttackDamage = 60;
	rangedAttackDamage = 5;
	armorDamageReduction = 0;
}

SuperTrap::SuperTrap(const SuperTrap& superTrap)
{
	cout << "=== Copy SuperTrap constructor ===" << endl;
	*this = superTrap;
}

SuperTrap::~SuperTrap()
{
	cout << "=== SuperTrap destructor ===" << endl;
}

SuperTrap&
SuperTrap::operator=(const SuperTrap& superTrap)
{
	ClapTrap::operator=(superTrap);
	return (*this);
}
