/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/05 19:46:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

NinjaTrap::NinjaTrap()
{
	cout << "=== Default NinjaTrap constructor ===" << endl;
	hitPoints = 60;
	maxHitPoints = 60;
	energyPoints = 120;
	maxEnergyPoints = 120;
	level = 1;
	meleeAttackDamage = 60;
	rangedAttackDamage = 5;
	armorDamageReduction = 0;
}

NinjaTrap::NinjaTrap(const string& name) : ClapTrap(name)
{
	cout << "=== Parametrized NinjaTrap constructor ===" << endl;
	hitPoints = 60;
	maxHitPoints = 60;
	energyPoints = 120;
	maxEnergyPoints = 120;
	level = 1;
	meleeAttackDamage = 60;
	rangedAttackDamage = 5;
	armorDamageReduction = 0;
}

NinjaTrap::NinjaTrap(const NinjaTrap& ninjaTrap)
{
	cout << "=== Copy NinjaTrap constructor ===" << endl;
	*this = ninjaTrap;
}

NinjaTrap::~NinjaTrap()
{
	cout << "=== NinjaTrap destructor ===" << endl;
}

NinjaTrap&
NinjaTrap::operator=(const NinjaTrap& ninjaTrap)
{
	ClapTrap::operator=(ninjaTrap);
	return (*this);
}

void
NinjaTrap::ninjaShoebox(ClapTrap& clapTrap)
{
	if (energyPoints < 10)
	{
		cout << "N1NJ4-TP " << *name << " doesn't have enery points for";
		cout << " ninjaShoebox" << endl;
		return;
	}
	energyPoints -= 10;

	cout << "N1NJ4-TP " << *name << " uses ninjaShoebox on CL4P-TP ";
	cout << clapTrap.getName() << " and it gets repaired because yes" << endl;

	clapTrap.beRepaired(1000);
}

void
NinjaTrap::ninjaShoebox(FragTrap& fragTrap)
{
	if (energyPoints < 10)
	{
		cout << "N1NJ4-TP " << *name << " doesn't have enery points for";
		cout << " ninjaShoebox" << endl;
		return;
	}
	energyPoints -= 10;

	cout << "N1NJ4-TP " << *name << " uses ninjaShoebox on FR4G-TP ";
	cout << fragTrap.getName() << " and it gets annihilated" << endl;

	fragTrap.takeDamage(1000);
}

void
NinjaTrap::ninjaShoebox(ScavTrap& scavTrap)
{
	if (energyPoints < 10)
	{
		cout << "N1NJ4-TP " << *name << " doesn't have enery points for";
		cout << " ninjaShoebox" << endl;
		return;
	}
	energyPoints -= 10;

	cout << "N1NJ4-TP " << *name << " uses ninjaShoebox on SC4V-TP ";
	cout << scavTrap.getName() << " and now has a better armor" << endl;

	scavTrap.incrementDamageReduction(5);
}

void
NinjaTrap::ninjaShoebox(NinjaTrap& ninjaTrap)
{
	if (energyPoints < 10)
	{
		cout << "N1NJ4-TP " << *name << " doesn't have enery points for";
		cout << " ninjaShoebox" << endl;
		return;
	}
	energyPoints -= 10;

	cout << "N1NJ4-TP " << *name << " uses ninjaShoebox on N1NJ4-TP ";
	cout << ninjaTrap.getName() << " and it leaves it alone!!!!!!!" << endl;
}
