/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/05 17:47:22 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

FragTrap::FragTrap()
{
	cout << "=== Default FragTrap constructor ===" << endl;
	srand(time(NULL));
	hitPoints = 100;
	maxHitPoints = 100;
	energyPoints = 100;
	maxEnergyPoints = 100;
	level = 1;
	meleeAttackDamage = 30;
	rangedAttackDamage = 20;
	armorDamageReduction = 5;
}

FragTrap::FragTrap(const string& name) : ClapTrap(name)
{
	cout << "=== Parametrized FragTrap constructor ===" << endl;
	hitPoints = 100;
	maxHitPoints = 100;
	energyPoints = 100;
	maxEnergyPoints = 100;
	level = 1;
	meleeAttackDamage = 30;
	rangedAttackDamage = 20;
	armorDamageReduction = 5;
}

FragTrap::FragTrap(const FragTrap& fragTrap)
{
	cout << "=== Copy FragTrap constructor ===" << endl;
	*this = fragTrap;
}

FragTrap::~FragTrap()
{
	cout << "=== FragTrap destructor ===" << endl;
}

FragTrap&
FragTrap::operator=(const FragTrap& fragTrap)
{
	ClapTrap::operator=(fragTrap);
	return (*this);
}

void
FragTrap::vaulthunter_dot_exe(const string& target)
{
	if (energyPoints < 25)
	{
		cout << "FR4G-TP " << *name << " doesn't have enery points for";
		cout << " vaulthunter.exe" << endl;
		return;
	}
	energyPoints -= 25;

	cout << "FR4G-TP " << *name << " attacks FR4G-TP " << target;
	switch (rand() % 5)
	{
		case 0:
			cout << " with a wooden stick causing 5 points of damage!" << endl;
			break;
		case 1:
			cout << " with a memory leak causing 10 points of damage!" << endl;
			break;
		case 2:
			cout << " with a segmentation fault causing 15 points of damage!" << endl;
			break;
		case 3:
			cout << " with Doraemon causing 20 points of damage!" << endl;
			break;
		default:
			cout << " with the magic of friendship causing 25 points of damage!";
			cout << endl;
	}
}
