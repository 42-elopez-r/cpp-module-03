/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 17:46:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/11 20:08:13 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJA_TRAP_HPP
#define NINJA_TRAP_HPP

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include <string>

using std::string;

class NinjaTrap: public virtual ClapTrap
{
	public:
		NinjaTrap();
		NinjaTrap(const string& name);
		NinjaTrap(const NinjaTrap& ninjaTrap);
		~NinjaTrap();
		NinjaTrap& operator=(const NinjaTrap& ninjaTrap);
		void ninjaShoebox(ClapTrap& clapTrap);
		void ninjaShoebox(FragTrap& fragTrap);
		void ninjaShoebox(ScavTrap& scavTrap);
		void ninjaShoebox(NinjaTrap& ninjaTrap);
};

#endif
