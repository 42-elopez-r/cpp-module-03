/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/05 17:49:16 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

ScavTrap::ScavTrap()
{
	cout << "=== Default ScavTrap constructor ===" << endl;
	srand(time(NULL));
	hitPoints = 100;
	maxHitPoints = 100;
	energyPoints = 50;
	maxEnergyPoints = 50;
	level = 1;
	meleeAttackDamage = 20;
	rangedAttackDamage = 15;
	armorDamageReduction = 3;
}

ScavTrap::ScavTrap(const string& name) : ClapTrap(name)
{
	cout << "=== Parametrized ScavTrap constructor ===" << endl;
	hitPoints = 100;
	maxHitPoints = 100;
	energyPoints = 50;
	maxEnergyPoints = 50;
	level = 1;
	meleeAttackDamage = 20;
	rangedAttackDamage = 15;
	armorDamageReduction = 3;
}

ScavTrap::ScavTrap(const ScavTrap& scavTrap)
{
	cout << "=== Copy ScavTrap constructor ===" << endl;
	*this = scavTrap;
}

ScavTrap::~ScavTrap()
{
	cout << "=== ScavTrap destructor ===" << endl;
}

ScavTrap&
ScavTrap::operator=(const ScavTrap& scavTrap)
{
	ClapTrap::operator=(scavTrap);
	return (*this);
}

void
ScavTrap::challengeNewcomer(const string& target)
{
	if (energyPoints < 25)
	{
		cout << "SC4V-TP " << *name << " doesn't have enery points to";
		cout << " challenge " << target << endl;
		return;
	}
	energyPoints -= 25;

	cout << "SC4V-TP " << *name << " challenges " << target;
	cout << " with the enigma: ";
	switch (rand() % 5)
	{
		case 0:
			cout << "what was first, the egg or the chicken?" << endl;
			break;
		case 1:
			cout << "how old is Celestia?" << endl;
			break;
		case 2:
			cout << "does this program makes any sense?" << endl;
			break;
		case 3:
			cout << "d2hhdCBhbSBJIHNheWluZwo= ?" << endl;
			break;
		default:
			cout << "will it rain tomorrow?" << endl;
	}
}
