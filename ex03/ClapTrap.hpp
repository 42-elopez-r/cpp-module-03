/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 17:46:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/05 19:37:04 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAP_TRAP_HPP
#define CLAP_TRAP_HPP

#include <string>

using std::string;

class ClapTrap
{
	protected:
		unsigned int hitPoints;
		unsigned int maxHitPoints;
		unsigned int energyPoints;
		unsigned int maxEnergyPoints;
		unsigned int level;
		string *name;
		unsigned int meleeAttackDamage;
		unsigned int rangedAttackDamage;
		unsigned int armorDamageReduction;
	public:
		ClapTrap();
		ClapTrap(const string& name);
		ClapTrap(const ClapTrap& clapTrap);
		~ClapTrap();
		ClapTrap& operator=(const ClapTrap& clapTrap);
		const string& getName() const;
		void incrementDamageReduction(unsigned int amount);
		void rangedAttack(const string& target);
		void meleeAttack(const string& target);
		void takeDamage(unsigned int amount);
		void beRepaired(unsigned int amount);
};

#endif
