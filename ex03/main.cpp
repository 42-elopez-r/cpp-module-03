/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 19:39:30 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/05 19:30:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include <iostream>

using std::cout; using std::endl;

int
main()
{
	NinjaTrap n1("N1"), n2("N2");
	ClapTrap c1("C1");
	ScavTrap s1("S1");
	FragTrap f1("F1");

	n1.ninjaShoebox(c1);
	n1.ninjaShoebox(s1);
	n1.ninjaShoebox(f1);
	n1.ninjaShoebox(n2);

	return (0);
}
