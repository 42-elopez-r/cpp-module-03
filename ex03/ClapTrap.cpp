/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/06/05 19:38:07 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include <iostream>
#include <ctime>

using std::cout; using std::endl;

ClapTrap::ClapTrap()
{
	cout << "=== Default ClapTrap constructor ===" << endl;
	hitPoints = 0;
	maxHitPoints = 0;
	energyPoints = 0;
	maxEnergyPoints = 0;
	level = 0;
	name = new string("unnamed");
	meleeAttackDamage = 0;
	rangedAttackDamage = 0;
	armorDamageReduction = 0;
}

ClapTrap::ClapTrap(const string& name)
{
	cout << "=== Parametrized ClapTrap constructor ===" << endl;
	this->name = NULL;
	*this = ClapTrap();
	delete this->name;
	this->name = new string(name);
}

ClapTrap::ClapTrap(const ClapTrap& clapTrap)
{
	cout << "=== Copy ClapTrap constructor ===" << endl;
	*this = clapTrap;
}

ClapTrap::~ClapTrap()
{
	cout << "=== ClapTrap destructor ===" << endl;
	delete name;
}

ClapTrap&
ClapTrap::operator=(const ClapTrap& clapTrap)
{
	this->hitPoints = clapTrap.hitPoints;
	this->maxHitPoints = clapTrap.maxHitPoints;
	this->energyPoints = clapTrap.energyPoints;
	this->maxEnergyPoints = clapTrap.maxHitPoints;
	this->level = clapTrap.level;
	delete this->name;
	this->name = new string(*clapTrap.name);
	this->meleeAttackDamage = clapTrap.meleeAttackDamage;
	this->rangedAttackDamage = clapTrap.rangedAttackDamage;
	this->armorDamageReduction = clapTrap.armorDamageReduction;
	return (*this);
}

const string&
ClapTrap::getName() const
{
	return (*name);
}

void
ClapTrap::incrementDamageReduction(unsigned int amount)
{
	armorDamageReduction += amount;
}

void
ClapTrap::rangedAttack(const string& target)
{
	cout << "CL4P-TP " << *name << " attacks " << target << " at range, causing ";
	cout << rangedAttackDamage << " points of damage!" << endl;
}

void
ClapTrap::meleeAttack(const string& target)
{
	cout << "CL4P-TP " << *name << " attacks " << target << " with melee, causing ";
	cout << meleeAttackDamage << " points of damage!" << endl;
}

void
ClapTrap::takeDamage(unsigned int amount)
{
	unsigned int damageActuallyTaken;

	damageActuallyTaken = amount < armorDamageReduction ?
		0 : amount - armorDamageReduction;
	cout << "CL4P-TP " << *name << " takes " << amount << " points of damage";
	cout << " but the armor reduces it to " << damageActuallyTaken << endl;
	if (damageActuallyTaken > hitPoints)
	{
		cout << *name << " is DED." << endl;
		hitPoints = 0;
	}
	else
		hitPoints -= damageActuallyTaken;
}

void
ClapTrap::beRepaired(unsigned int amount)
{
	if (amount >= maxHitPoints)
	{
		cout << "CL4P-TP " << *name << " is completely repaired" << endl;
		hitPoints = maxHitPoints;
	}
	else
	{
		cout << "CL4P-TP " << *name << " repaired with " << amount << " points" << endl;
		hitPoints += amount;
	}
}
