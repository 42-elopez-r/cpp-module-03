/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/30 20:08:59 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

FragTrap::FragTrap()
{
	cout << "=== Default FragTrap constructor ===" << endl;
	srand(time(NULL));
	hitPoints = 100;
	maxHitPoints = 100;
	energyPoints = 100;
	maxEnergyPoints = 100;
	level = 1;
	name = new string("unnamed");
	meleeAttackDamage = 30;
	rangedAttackDamage = 20;
	armorDamageReduction = 5;
}

FragTrap::FragTrap(const string& name)
{
	cout << "=== Parametrized FragTrap constructor ===" << endl;
	this->name = NULL;
	*this = FragTrap();
	delete this->name;
	this->name = new string(name);
}

FragTrap::FragTrap(const FragTrap& fragTrap)
{
	cout << "=== Copy FragTrap constructor ===" << endl;
	*this = fragTrap;
}

FragTrap::~FragTrap()
{
	cout << "=== FragTrap destructor ===" << endl;
	delete name;
}

FragTrap&
FragTrap::operator=(const FragTrap& fragTrap)
{
	this->hitPoints = fragTrap.hitPoints;
	this->maxHitPoints = fragTrap.maxHitPoints;
	this->energyPoints = fragTrap.energyPoints;
	this->maxEnergyPoints = fragTrap.maxHitPoints;
	this->level = fragTrap.level;
	delete this->name;
	this->name = new string(*fragTrap.name);
	this->meleeAttackDamage = fragTrap.meleeAttackDamage;
	this->rangedAttackDamage = fragTrap.rangedAttackDamage;
	this->armorDamageReduction = fragTrap.armorDamageReduction;
	return (*this);
}

void
FragTrap::rangedAttack(const string& target)
{
	cout << "FR4G-TP " << *name << " attacks " << target << " at range, causing ";
	cout << rangedAttackDamage << " points of damage!" << endl;
}

void
FragTrap::meleeAttack(const string& target)
{
	cout << "FR4G-TP " << *name << " attacks " << target << " with melee, causing ";
	cout << meleeAttackDamage << " points of damage!" << endl;
}

void
FragTrap::takeDamage(unsigned int amount)
{
	unsigned int damageActuallyTaken;

	damageActuallyTaken = amount < armorDamageReduction ?
		0 : amount - armorDamageReduction;
	cout << "FR4G-TP " << *name << " takes " << amount << " points of damage";
	cout << " but the armor reduces it to " << damageActuallyTaken << endl;
	if (damageActuallyTaken > hitPoints)
	{
		cout << *name << " is DED." << endl;
		hitPoints = 0;
	}
	else
		hitPoints -= damageActuallyTaken;
}

void
FragTrap::beRepaired(unsigned int amount)
{
	if (amount >= maxHitPoints)
	{
		cout << "FR4G-TP " << *name << " is completely repaired" << endl;
		hitPoints = maxHitPoints;
	}
	else
	{
		cout << "FR4G-TP " << *name << " repaired with " << amount << " points" << endl;
		hitPoints += amount;
	}
}

void
FragTrap::vaulthunter_dot_exe(const string& target)
{
	if (energyPoints < 25)
	{
		cout << "FR4G-TP " << *name << " doesn't have enery points for";
		cout << " vaulthunter.exe" << endl;
		return;
	}
	energyPoints -= 25;

	cout << "FR4G-TP " << *name << " attacks FR4G-TP " << target;
	switch (rand() % 5)
	{
		case 0:
			cout << " with a wooden stick causing 5 points of damage!" << endl;
			break;
		case 1:
			cout << " with a memory leak causing 10 points of damage!" << endl;
			break;
		case 2:
			cout << " with a segmentation fault causing 15 points of damage!" << endl;
			break;
		case 3:
			cout << " with Doraemon causing 20 points of damage!" << endl;
			break;
		default:
			cout << " with the magic of friendship causing 25 points of damage!";
			cout << endl;
	}
}
