/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 19:39:30 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/31 20:02:55 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

int
main()
{
	FragTrap f1("F1");
	FragTrap f2("F2");
	FragTrap f3;

	f3 = f2;
	f1.rangedAttack("F2");
	f2.meleeAttack("F3");
	f3.takeDamage(20);
	f3.takeDamage(200);
	f3.beRepaired(20);
	f3.beRepaired(200);
	for (int i = 0; i < 6; i++)
		f1.vaulthunter_dot_exe("F2");
	return (0);
}
