/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 19:39:30 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/31 19:57:43 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include <iostream>

using std::cout; using std::endl;

static void
testFragTrap()
{
	FragTrap f1("F1");
	FragTrap f2("F2");
	FragTrap f2Copy;

	f2Copy = f2;
	f1.rangedAttack("F2");
	f2.meleeAttack("F1");
	f2Copy.takeDamage(20);
	f2Copy.takeDamage(200);
	f2Copy.beRepaired(20);
	f2Copy.beRepaired(200);
	for (int i = 0; i < 6; i++)
		f1.vaulthunter_dot_exe("F2");
}

static void
testScavTrap()
{
	ScavTrap s1("S1");
	ScavTrap s2("S2");
	ScavTrap s2Copy;

	s2Copy = s2;
	s1.rangedAttack("S2");
	s2.meleeAttack("S1");
	s2Copy.takeDamage(20);
	s2Copy.takeDamage(200);
	s2Copy.beRepaired(20);
	s2Copy.beRepaired(200);
	for (int i = 0; i < 6; i++)
		s1.challengeNewcomer("S2");
}

int
main()
{
	cout << "**** FRAG TRAP ****" << endl;
	testFragTrap();
	cout << "**** SCAV TRAP ****" << endl;
	testScavTrap();
	return (0);
}
