/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/23 18:06:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/31 19:48:34 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

ScavTrap::ScavTrap()
{
	cout << "=== Default ScavTrap constructor ===" << endl;
	srand(time(NULL));
	hitPoints = 100;
	maxHitPoints = 100;
	energyPoints = 50;
	maxEnergyPoints = 50;
	level = 1;
	name = new string("unnamed");
	meleeAttackDamage = 20;
	rangedAttackDamage = 15;
	armorDamageReduction = 3;
}

ScavTrap::ScavTrap(const string& name)
{
	cout << "=== Parametrized ScavTrap constructor ===" << endl;
	this->name = NULL;
	*this = ScavTrap();
	delete this->name;
	this->name = new string(name);
}

ScavTrap::ScavTrap(const ScavTrap& scavTrap)
{
	cout << "=== Copy ScavTrap constructor ===" << endl;
	*this = scavTrap;
}

ScavTrap::~ScavTrap()
{
	cout << "=== ScavTrap destructor ===" << endl;
	delete name;
}

ScavTrap&
ScavTrap::operator=(const ScavTrap& scavTrap)
{
	this->hitPoints = scavTrap.hitPoints;
	this->maxHitPoints = scavTrap.maxHitPoints;
	this->energyPoints = scavTrap.energyPoints;
	this->maxEnergyPoints = scavTrap.maxHitPoints;
	this->level = scavTrap.level;
	delete this->name;
	this->name = new string(*scavTrap.name);
	this->meleeAttackDamage = scavTrap.meleeAttackDamage;
	this->rangedAttackDamage = scavTrap.rangedAttackDamage;
	this->armorDamageReduction = scavTrap.armorDamageReduction;
	return (*this);
}

void
ScavTrap::rangedAttack(const string& target)
{
	cout << "SC4V-TP " << *name << " attacks " << target << " at range, causing ";
	cout << rangedAttackDamage << " points of damage!" << endl;
}

void
ScavTrap::meleeAttack(const string& target)
{
	cout << "SC4V-TP " << *name << " attacks " << target << " with melee, causing ";
	cout << meleeAttackDamage << " points of damage!" << endl;
}

void
ScavTrap::takeDamage(unsigned int amount)
{
	unsigned int damageActuallyTaken;

	damageActuallyTaken = amount < armorDamageReduction ?
		0 : amount - armorDamageReduction;
	cout << "SC4V-TP " << *name << " takes " << amount << " points of damage";
	cout << " but the armor reduces it to " << damageActuallyTaken << endl;
	if (damageActuallyTaken > hitPoints)
	{
		cout << *name << " is DED." << endl;
		hitPoints = 0;
	}
	else
		hitPoints -= damageActuallyTaken;
}

void
ScavTrap::beRepaired(unsigned int amount)
{
	if (amount >= maxHitPoints)
	{
		cout << "SC4V-TP " << *name << " is completely repaired" << endl;
		hitPoints = maxHitPoints;
	}
	else
	{
		cout << "SC4V-TP " << *name << " repaired with " << amount << " points";
		cout << endl;
		hitPoints += amount;
	}
}

void
ScavTrap::challengeNewcomer(const string& target)
{
	if (energyPoints < 25)
	{
		cout << "SC4V-TP " << *name << " doesn't have enery points to";
		cout << " challenge " << target << endl;
		return;
	}
	energyPoints -= 25;

	cout << "SC4V-TP " << *name << " challenges " << target;
	cout << " with the enigma: ";
	switch (rand() % 5)
	{
		case 0:
			cout << "what was first, the egg or the chicken?" << endl;
			break;
		case 1:
			cout << "how old is Celestia?" << endl;
			break;
		case 2:
			cout << "does this program makes any sense?" << endl;
			break;
		case 3:
			cout << "d2hhdCBhbSBJIHNheWluZwo= ?" << endl;
			break;
		default:
			cout << "will it rain tomorrow?" << endl;
	}
}
